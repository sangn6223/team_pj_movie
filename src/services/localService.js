export const localUserService = {
  get: () => {
    let dataJson = localStorage.getItem("USER_LOGIN");
    return JSON.parse(dataJson);
  },
  set: (userInfor) => {
    let dataJson = JSON.stringify(userInfor);
    localStorage.setItem("USER_LOGIN", dataJson);
  },
  remove: () => {
    localStorage.removeItem("USER_LOGIN");
  },
};
