import axios from "axios";
import { BASE_URL, configHeader } from "./config";

export const movieService = {
  getMovieList: () => {
    return axios.get(`${BASE_URL}/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP07`, {
      headers: configHeader(),
    });
  },
  getMovieByTheater: () => {
    return axios.get(
      `${BASE_URL}/api/QuanLyRap/LayThongTinLichChieuHeThongRap?maNhom=GP02`,
      {
        headers: configHeader(),
      }
    );
  },
  getDetailMovie: (id) => {
    return axios({
      url: `${BASE_URL}/api/QuanLyPhim/LayThongTinPhim?MaPhim=${id}`,
      method: "GET",
      headers: configHeader(),
    });
  },
  QuanLyDatVe: (id) => {
    return axios({
      url: `${BASE_URL}/api/QuanLyDatVe/LayDanhSachPhongVe?MaLichChieu=44239`,
      method: "GET",
      headers: configHeader(),
    });
  },
};
