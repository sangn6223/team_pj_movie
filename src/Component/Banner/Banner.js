import React from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import banner_1 from "../../assets/img/banner/banner1.png";
import banner_2 from "../../assets/img/banner/banner3.png";
import banner_3 from "../../assets/img/banner/banner4.png";
import { NavLink } from "react-router-dom";
import Popup from "../../Pages/HomePage/PopupBanner/Popup";

export default function Banner() {
  const settings = {
    dots: true,
    infinite: true,
    speed: 400,
    autoplay: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: true,
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          initialSlide: 2,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  };
  return (
    <div className="" style={{ position: "relative" }}>
      <div>
        <Slider {...settings}>
          <div>
            <img style={{ width: "100%" }} src={banner_1} />
          </div>
          <div>
            <img style={{ width: "100%" }} src={banner_2} />
          </div>
          <div>
            <img style={{ width: "100%" }} src={banner_3} />
          </div>
        </Slider>
      </div>
      <div
        className="text-5xl"
        style={{
          position: "absolute",
          top: "50%",
          left: "50%",
          right: "auto",
          bottom: "auto",
          marginRight: "-50%",
          transform: "translate(-50%, -50%)",
          color: "#fff",
          border: "1px solid #fff",
          width: 100,
          height: 100,
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          borderRadius: "50%",
          backgroundColor: "rgb(21 21 21 / 70%)",
        }}
      >
        <Popup />
      </div>
    </div>
  );
}
