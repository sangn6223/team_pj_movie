import React from "react";
import { useSelector } from "react-redux";
import logo from "../../assets/img/logo/logo.png";
import { localUserService } from "../../services/localService";
import { NavLink } from "react-router-dom";

export default function HeaderDesktop() {
  let userInfor = useSelector((state) => {
    return state.userReducer.userInfor;
  });
  // log out user
  let handleLogout = () => {
    //remove local and log out to login
    localUserService.remove();
    window.location.href = "/";
  };
  let renderUserNav = () => {
    let btnCss = "px-5 py-2 rounded border-solid border border-black";
    if (userInfor) {
      // đã đăng nhập
      return (
        <>
          <span>{userInfor.hoTen}</span>
          <button onClick={handleLogout} className={btnCss}>
            Đăng Xuất
          </button>
        </>
      );
    } else {
      return (
        <>
          <NavLink to="/signup">
            <button className={btnCss}>Đăng ký</button>
          </NavLink>
          <NavLink to="/login">
            <button className={btnCss}>Đăng Nhập</button>
          </NavLink>
        </>
      );
    }
  };
  return (
    <div className="flex justify-between items-center h-20 container">
      <div className="flex items-center">
        <NavLink to="/">
          <img style={{ width: 70 }} src={logo} alt="logo" />
        </NavLink>
        <NavLink to="/">
          <span
            className="tracking-widest"
            style={{
              fontSize: "22px",
              display: "inline-block",
              marginTop: "35px",
            }}
          >
            CINEMA
          </span>
        </NavLink>
      </div>

      <div className="space-x-5">{renderUserNav()}</div>
    </div>
  );
}
