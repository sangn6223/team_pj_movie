import React from "react";
import logo from "../../assets/img/logo/logo.png";
import { localUserService } from "../../services/localService";
import { NavLink } from "react-router-dom";

export default function HeaderMobile() {
  return (
    <div className="flex justify-between items-center h-20 container">
      <div className="flex items-center">
        <NavLink to="/">
          <img style={{ width: 70 }} src={logo} alt="logo" />
        </NavLink>
        <NavLink to="/">
          <span
            className="tracking-widest"
            style={{
              fontSize: "22px",
              display: "inline-block",
              marginTop: "35px",
            }}
          >
            CINEMA
          </span>
        </NavLink>
      </div>

      <button className="text-2xl text-orange-500">
        <i class="fa fa-bars"></i>
      </button>
    </div>
  );
}
