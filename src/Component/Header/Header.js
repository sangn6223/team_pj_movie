import React from "react";
import {
  Desktop,
  LargeScreen,
  MediumScreen,
  Mobile,
  Tablet,
} from "../../Layouts/Responsive";
import HeaderDesktop from "./HeaderDesktop";
import HeaderMobile from "./HeaderMobile";

export default function Header() {
  return (
    <div>
      <LargeScreen>
        <HeaderDesktop />
      </LargeScreen>
      <MediumScreen>
        <HeaderMobile />
      </MediumScreen>
    </div>
  );
}
