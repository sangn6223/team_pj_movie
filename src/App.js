import "./App.css";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import HomePage from "./Pages/HomePage/HomePage";
import LoginPage from "./Pages/LoginPage.js/LoginPage";
import DetailPage from "./Pages/DetailPage/DetailPage";
import Layout from "./Layouts/Layout";
import FooterMovie from "./Component/Footer/FooterMovie";
import SignUp from "./Pages/LoginPage.js/SignUp";
import CheckOut from "./Pages/CheckOut/CheckOut";

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          <Route path="" element={<Layout Component={HomePage} />} />
          <Route
            path="/login"
            element={<Layout Component={LoginPage} />}
          />{" "}
          <Route path="/signup" element={<Layout Component={SignUp} />} />{" "}
          <Route
            path="/detail/:id"
            element={<Layout Component={DetailPage} />}
          />
          <Route path="/footer" element={<Layout Component={FooterMovie} />} />
          <Route path="/checkout" element={<Layout Component={CheckOut} />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
