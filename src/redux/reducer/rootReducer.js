import { combineReducers } from "redux";
import userReducer from "./userReducer";
import QuanLyDatVeReducer from "./quanLyDatVeReducer";
//-----------------------------------------
export const rootReducer = combineReducers({ userReducer, QuanLyDatVeReducer });
