import React from "react";
import { Form, Button, Checkbox, Input, message } from "antd";
import bg_login from "../../assets/img/LoginPage/bg-login.jpg";
import { userSerVice } from "../../services/userService";
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { USER_LOGIN } from "../../redux/constant/userConstant";
import { localUserService } from "../../services/localService";
import {
  setUserAction,
  setUserActionThunk,
} from "../../redux/action/UserAction";
//------------------------------------

//------------------------------------
export default function LoginPage() {
  let navigate = useNavigate(); // func dùng để chuyển trang
  let dispatch = useDispatch();
  const onFinish = (values) => {
    console.log("Success:", values);
    userSerVice // consn userSerVice chấm đến key postLogin(values) => truyền tham số values
      .postLogin(values)
      .then((res) => {
        // console.log(res);
        message.success("Success"); // message của thư viên antd
        localUserService.set(res.data.content); // lưu dữ liệu xuống
        //redux localStorge
        dispatch(setUserAction(res.data.content)); // đẩy lên redux và lưu về localStorge
        navigate("/home"); // đưa user về trang home
      })
      .catch((err) => {
        console.log(err);
        message.error("Đăng Nhập Thất Bại");
      });
  };
  // viết func onFinish mới
  const onFinishThunk = (values) => {
    let handleSuccess = (res) => {
      message.success("Đăng Nhập Thành Công"); // message của thư viên antd
      localUserService.set(res.data.content); // lưu dữ liệu xuống
      navigate("/"); // đưa user về trang home
    };
    dispatch(setUserActionThunk(values, handleSuccess));
  };
  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  return (
    <div
      className="w-screen h-screen flex items-center justify-center"
      style={{ backgroundImage: `url(${bg_login})`, backgroundSize: "cover" }}
    >
      <div
        className="rounded  w-5/12 py-10 mx-auto"
        style={{ backgroundColor: "rgb(202 240 248 / 40%)" }}
      >
        <h3 style={{ color: "#fff", fontSize: "32px" }}>Đăng Nhập</h3>

        <Form
          className="bg-white  mx-10 py-20 pr-10"
          name="basic"
          labelCol={{
            span: 6,
          }}
          wrapperCol={{
            span: 16,
          }}
          initialValues={{
            remember: true,
          }}
          onFinish={onFinishThunk}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
        >
          <Form.Item
            label="Username"
            name="taiKhoan"
            rules={[
              {
                required: true,
                message: "Please input your username!",
              },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            label="Password"
            name="matKhau"
            rules={[
              {
                required: true,
                message: "Please input your password!",
              },
            ]}
          >
            <Input.Password />
          </Form.Item>
          <Form.Item
            wrapperCol={{
              offset: 6,
              span: 16,
            }}
          >
            <Button
              type="primary"
              htmlType="submit"
              style={{ backgroundColor: "#0ea5e9", width: "100%" }}
            >
              Submit
            </Button>
          </Form.Item>
        </Form>
      </div>
    </div>
  );
}
