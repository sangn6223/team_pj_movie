import React from "react";
import { Card } from "antd";
import { NavLink } from "react-router-dom";
const { Meta } = Card;
export default function CardMovie({ movie }) {
  let { tenPhim, hinhAnh, moTa } = movie;
  return (
    <div className="container">
      <Card
        hoverable
        style={{
          width: "100%",
          margin: "20px 0",
        }}
        cover={
          <img
            style={{ height: 300, objectFit: "cover", objectPosition: "top" }}
            alt="example"
            src={hinhAnh}
          />
        }
      >
        {/* detail  */}
        <NavLink className={"text-red-600"} to={`/detail/${movie.maPhim}`}>
          Xem Chi Tiết
        </NavLink>
        <Meta style={{}} title={tenPhim} description={""} />
        <NavLink to="/checkout">
          <button className="bg-red-500 text-white py-2 px-7 mt-3 text-xl">
            Mua Vé
          </button>
        </NavLink>
      </Card>
    </div>
  );
}
