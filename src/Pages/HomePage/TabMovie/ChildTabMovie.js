import React from "react";
import moment from "moment";
export default function ChildTabMovie({ listMovie }) {
  return (
    <div style={{ height: 550, width: 400 }} className="overflow-y-auto ">
      {listMovie.map((phim) => {
        return (
          <div className="flex space-x-5">
            <img className="w-24 h-full my-1" src={phim.hinhAnh} />
            <div>
              <p className="text-lg font-bold text-left">{phim.tenPhim}</p>
              <div className="text-left text-blue-500 ">
                {phim.lstLichChieuTheoPhim.map((lich) => {
                  return (
                    <p
                      className="m-2 inline-block py-3 px-10 rounded border-solid border-2 border-gray-400-600 "
                      style={{ backgroundColor: "rgba(246, 246, 246, 0.9)" }}
                    >
                      {moment(lich.ngayChieuGioChieu).format("DD/MM/YY")}
                    </p>
                  );
                })}
              </div>
            </div>
          </div>
        );
      })}
    </div>
  );
}
