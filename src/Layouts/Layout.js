import React from "react";
import Header from "../Component/Header/Header";
import FooterMovie from "../Component/Footer/FooterMovie";
import DetailPage from "../Pages/DetailPage/DetailPage";
import SignUp from "../Pages/LoginPage.js/SignUp";

export default function Layout({ Component }) {
  // Component = Props => truyền tham số vào các page khác
  return (
    <div>
      <Header />
      <Component />
      <FooterMovie />
    </div>
  );
}
