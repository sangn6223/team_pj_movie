import { useMediaQuery } from "react-responsive";

export const LargeScreen = ({ children }) => {
  const isLargeScreen = useMediaQuery({ minWidth: 1280 });
  return isLargeScreen ? children : null;
};
export const MediumScreen = ({ children }) => {
  const isMediumScreen = useMediaQuery({ maxWidth: 1279 });
  return isMediumScreen ? children : null;
};
export const Desktop = ({ children }) => {
  const isDesktop = useMediaQuery({ minWidth: 992 });
  return isDesktop ? children : null;
};
export const Tablet = ({ children }) => {
  const isTablet = useMediaQuery({ minWidth: 768 });
  return isTablet ? children : null;
};
export const Mobile = ({ children }) => {
  const isMobile = useMediaQuery({ maxWidth: 767 });
  return isMobile ? children : null;
};
export const Default = ({ children }) => {
  const isNotMobile = useMediaQuery({ minWidth: 768 });
  return isNotMobile ? children : null;
};
